import React from 'react';
import mapa from './mapa.jpg';
import styled from 'styled-components';

const Img = styled.img`
  width: 25%;
`;

class Mapa extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Img src={mapa}/>
    );
  }
}
Mapa.propTypes = {

};

export default Mapa;
