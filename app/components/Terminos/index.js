import styled from 'styled-components';
import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

  const Ul = styled.ul`
    list-style: none;
  `;

  const Terminosforma = styled.div`
  height: auto;
  width: 65%;
  text-align: justify;
  padding: 10px;
  border: 2px solid #ecf0f1;
  `;

  const H3 = styled.h4`
    font-family: sans-serif;
    color: #3498db;
  `;

  const P = styled.p`
    font-family: sans-serif;
    margin-top: -15px;
    color: #7f8c8d;
  `;

function Terminos() {
  return (
    <Terminosforma>
      <Ul>
        <li><H3>Terminos y condiciones</H3></li>
        <li><P>Todos los importes incluyen los impuestos a menos que se especifique lo contrario. Le confirmamos haber recibido el pago correspondiente a esta reservacion, y en el caso de haber pagado con una tarjeta de credito, la siguiente leyenda aparecera en el estado de cuenta de su tarjeta de credito: "HOLTEDO" o el NOMBRE de la pagina de internet en donde realizo su reservacion.</P></li>
       <li><P>Paquetes con Avion no son reembolsables, cancelables o transferibles. NO aplican cambios de nombres.</P></li>
       <li><H3>Politicas de cancelacion</H3></li>
       <li><P>Permite cancelar sin cargo hasta 8 dias antes de la fecha de llegada, de 7 dias a 24 horas antes de su llegada, aplica 2 noches de estancia. En caso de No Show, o salida anticipada queda sujeto al cobro por el total.</P></li>
      </Ul>
    </Terminosforma>
  );
}

Terminos.propTypes = {

};

export default Terminos;
