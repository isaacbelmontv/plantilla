/*
 * Terminos Messages
 *
 * This contains all the text for the Terminos component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Terminos.header',
    defaultMessage: 'This is the Terminos component !',
  },
});
