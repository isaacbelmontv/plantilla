import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Ul = styled.ul`
  width: 79%;
  margin-top: 0px;
  list-style: none;
  background-color: #2980b9;
  padding-top: 7px;
  padding-bottom: 7px;
  display: inline-block;
  text-decoration: none;
  color: #fff;
  font-weight: bold;
  display: flex;
  flex-direction: row;
`;

const Li = styled.li`
  margin-left: 200px;
`;

const Litipo = styled.li`
  margin-left: 80px;
`;

function Textnav() {
  return (
      <Ul>
        <Litipo>Tipo de habitación</Litipo>
        <Li>Características</Li>
      </Ul>
  );
}
Textnav.propTypes = {

};
export default Textnav;
