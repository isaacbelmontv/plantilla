/*
 * Textnav Messages
 *
 * This contains all the text for the Textnav component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Textnav.header',
    defaultMessage: 'This is the Textnav component !',
  },
});
