import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

{
/*
ejemplo
const Ul = styled.ul`
  width: 79%;
  margin-top: 0px;
  list-style: none;
  background-color: #2980b9;
  padding-top: 7px;
  padding-bottom: 7px;
  display: inline-block;
  text-decoration: none;
  color: #fff;
  font-weight: bold;
  display: flex;
  flex-direction: row;
`;
*/
}

const Padre = styled.div`
  width: 79%;
  margin-top: 0px;
  list-style: none;
  display: flex;
  flex-direction: row;
`;

const H2 = styled.h3`
  font-family: sans-serif;
  color: #3498db;
  margin-left: 30px;
`;

function SunsetWater() {
  return (
    <Padre>
      <div>
        <H2>Villa Sunset Water</H2>
      </div>
    </Padre>

    /*
    ejemplo ingresar datos
    <Ul>
      <Litipo>Tipo de habitación</Litipo>
      <Li>Características</Li>
    </Ul>
    */

  );
}

SunsetWater.propTypes = {

};

export default SunsetWater;
