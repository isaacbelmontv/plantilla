/*
 * SunsetWater Messages
 *
 * This contains all the text for the SunsetWater component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.SunsetWater.header',
    defaultMessage: 'This is the SunsetWater component !',
  },
});
