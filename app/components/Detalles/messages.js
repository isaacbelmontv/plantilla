/*
 * Detalles Messages
 *
 * This contains all the text for the Detalles component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Detalles.header',
    defaultMessage: 'This is the Detalles component !',
  },
});
