import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Details = styled.div`
background-color: #ecf0f1;
height: auto;
width: 50%;
text-align: justify;
padding: 10px;
`;

const H3 = styled.h4`
  font-family: sans-serif;
  color: #3498db;
`;

const Ul = styled.ul`
  list-style: none;
`;

const Li = styled.li`
  list-style: none;
`;

const H4 = styled.h4`
  font-family: sans-serif;
  color: #3498db;
`;

const H5 = styled.h4`
  font-family: sans-serif;
  color: #3498db;
  margin-left:5px;
`;

const P = styled.p`
  font-family: sans-serif;
  margin-top: -15px;
  color: #7f8c8d;
`;



const Check = styled.ul`
display: flex;
flex-direction: row;
`;

const Ins = styled.li`
  list-style: none;
`;
const Num = styled.p`
  margin: 20px 0px 0px 5px;
  font-family: sans-serif;
  color: #7f8c8d;
`;

function Detalles() {
  return (
    <Details>
      <Ul>
        <li><H3>Detalles de la estacia</H3></li>
        <li><P>Habitacion: Individual Economica</P></li>
        <li><P>Plan de Alimentos: Todo incluido</P></li>
        <li><P>Cama: Doble</P></li>
        <li><P>Zona: Cerca del aeropuerto</P></li>
        <li><P>Tipo de tarifa: Reembolsable</P></li>
      </Ul>
      <Check>
        <Ins><H4>Check In:</H4></Ins>
        <Ins><Num>02:00pm</Num></Ins>
        <Ins><H5>Check Out:</H5></Ins>
        <Ins><Num>12:00pm</Num></Ins>
      </Check>
      <Ul>
        <li><H3>Informacion importante del hotel</H3></li>
        <li><P>Favor de cobrar ningun importe por los servicios
        arriba detallados. La reservacion esta pagada y
        confirmada</P></li>
      </Ul>
    </Details>
  );
}

Detalles.propTypes = {

};

export default Detalles;
