import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const FlexIn = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 15px;
`;

const Flexint = styled.div`
  display: flex;
  flex-direction: row;
`;

const Inputt = styled.input`
  padding: 5px 8px 5px 8px;
  border: 1px solid #bdc3c7;
  width: 130px;
`;

const Label = styled.label`
  font-family: sans-serif;
  color: #7f8c8d;
`;

class Input extends React.PureComponent {
  render() {
    return (
      <Flexint>
        <FlexIn>
          <Label>Destino</Label>
          <Inputt placeholder="Destino" type="text"/>
        </FlexIn>
        <FlexIn>
          <Label>Entrada</Label>
          <Inputt placeholder="dd/mm/aa" type="text"/>
        </FlexIn>
        <FlexIn>
          <Label>Salida</Label>
          <Inputt placeholder="dd/mm/aa" type="text"/>
        </FlexIn>
      </Flexint>
    );
  }
}

Input.propTypes = {

};

export default Input;
