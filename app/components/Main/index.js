import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import imagen from './vacaciones.jpg';

const Div = styled.div`
  width: 79%;
  margin: 10px 0px 0px 0px;
`;

const Jumeirah = styled.div`
  display: flex;
  flex-direction: row;
  border: 2px solid #ecf0f1;
`;

const Img = styled.img`
  width: 44%;
`;

const Ul = styled.ul`
  list-style: none;
`;

const Li = styled.li`
  list-style: none;
`;

const H2 = styled.h2`
  font-family: sans-serif;
  color: #3498db;
`;

const P = styled.p`
  font-family: sans-serif;
  margin-top: -15px;
  color: #7f8c8d;
`;

const Mal = styled.p`
  font-family: sans-serif;
  margin-top: -10px;
  color: #34495e;
`;

const Det = styled.p`
  font-family: sans-serif;
  margin-top: -10px;
  color: #34495e;
  margin-left: 420px;
`;

const Star = styled.i`
  color: #f1c40f;
  margin-right: 5px;
`;

const Circle = styled.i`
  color: #27ae60;
  margin: 0px 5px 10px 0px;
`;

const Tripadvisor = styled.i`
  color: #2c3e50;
  margin-left: 10px;
  margin-right: 10px;
  margin-top: 10px;
  margin-bottom: 10px;
`;

const ChevronDown = styled.i`
  color: #2ecc71;
`;

const Mapmarker = styled.i`
  color: #2ecc71;
  margin-left: 5px;
`;

class Main extends React.PureComponent {
  render() {
    return (
      <Div>
        <Jumeirah>
          <Img src={imagen}/>
          <Ul>
            <Li><H2>Jumeirah Vittaveli</H2></Li>
            <Li><P>
              <Star className="fa fa-star" aria-hidden="true"></Star>
              <Star className="fa fa-star" aria-hidden="true"></Star>
              <Star className="fa fa-star" aria-hidden="true"></Star>
              <Star className="fa fa-star" aria-hidden="true"></Star>
              <Star className="fa fa-star" aria-hidden="true"></Star>
            </P></Li>
            <Li><P>Maldivas</P></Li>
            <Li><P>South Male Atoll, 20219 male South, Asian Boulevard</P></Li>
            <Li><P>Tel: 01 800 000 000</P></Li>
            <Li>
              <Tripadvisor className="fa fa-tripadvisor fa-2x" aria-hidden="true"></Tripadvisor>
              <Circle className="fa fa-dot-circle-o" aria-hidden="true"></Circle>
              <Circle className="fa fa-dot-circle-o" aria-hidden="true"></Circle>
              <Circle className="fa fa-dot-circle-o" aria-hidden="true"></Circle>
              <Circle className="fa fa-dot-circle-o" aria-hidden="true"></Circle>
              <Circle className="fa fa-circle-o" aria-hidden="true"></Circle>
            </Li>
            <Li><Mal>Maldivas, Asia<Mapmarker className="fa fa-map-marker" aria-hidden="true"></Mapmarker></Mal></Li>
            <Li><Det>Detalles <ChevronDown className="fa fa-chevron-down" aria-hidden="true"></ChevronDown></Det></Li>
          </Ul>
        </Jumeirah>
      </Div>
    );
  }
}

Main.propTypes = {
};

export default Main;
