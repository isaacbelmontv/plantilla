/*
 * Aside Messages
 *
 * This contains all the text for the Aside component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Aside.header',
    defaultMessage: 'This is the Aside component !',
  },
});
