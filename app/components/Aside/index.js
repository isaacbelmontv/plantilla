import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Div = styled.div`
  display: flex;
  flex-direction: column;
  border: 2px solid #ecf0f1;
  text-align: rigth;
  margin: 10px 0px 0px 0px;
  padding: 0px 20px 10px 20px;
`;

const Ul = styled.ul`
  display: flex;
  justify-content: flex-end;
  list-style: none;
`;

const Icon = styled.i`
  color: #2ecc71;
  margin: 0px 5px 0px 0px;
`;

const Input = styled.input`
  border: 1px solid #bdc3c7;
  width: 200px;
  color: #bcbcbc;
  padding-left: 30px;
  font-style: italic;
`;

const User = styled.i`
  margin: 0px 0px -19px 5px;
  color: #2ecc71;
`;

const A = styled.a`
  color: #7f8c8d;
  text-decoration: none;
  margin: 10px 0px 10px 0px;
`;

const Plus = styled.i`
  color: #2980b9;
`;

const Textarea = styled.textarea`
  border: 1px solid #bdc3c7;
  width: 200px;
  color: #bcbcbc;
  padding: 10px;
  font-style: italic;
`;

function Aside() {
  return (
    <div>
      <Div>
        <Ul>
          <li>
            <a href="#"><Icon className="fa fa-envelope" aria-hidden="true"></Icon></a>
            <a href="#"><Icon className="fa fa-print" aria-hidden="true"></Icon></a>
            <a href="#"><Icon className="fa fa-download" aria-hidden="true"></Icon></a>
          </li>
        </Ul>
        <User className="fa fa-envelope"></User>
        <Input type="text" placeholder="tucorreo@aqui.com"/>
        <A href="#"><Plus className="fa fa-plus-circle" aria-hidden="true"></Plus> Anadir otra habitacion</A>
        <Textarea rows="5" cols="40" placeholder="Comentarios"></Textarea>
      </Div>
    </div>
  );
}

Aside.propTypes = {

};

export default Aside;
