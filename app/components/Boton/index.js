import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const A = styled.a`
  background-color: #2980b9;
  padding-top: 7px;
  padding-right: 20px;
  padding-bottom: 7px;
  padding-left: 30px;
  display: inline-block;
  width: 240px;
  text-decoration: none;
  color: #fff;
  font-weight: bold;

  &:hover {
    background-color: #3498db;
    }
`;

class Boton extends React.PureComponent {
  render() {
    return (
      <A href="#">Busqueda Nueva <li className="fa fa-search" aria-hidden="true"></li></A>
    );
  }
}

Boton.propTypes = {

};

export default Boton;
