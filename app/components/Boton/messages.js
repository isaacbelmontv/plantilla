/*
 * Boton Messages
 *
 * This contains all the text for the Boton component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Boton.header',
    defaultMessage: 'This is the Boton component !',
  },
});
