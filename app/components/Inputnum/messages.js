/*
 * Inputnum Messages
 *
 * This contains all the text for the Inputnum component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Inputnum.header',
    defaultMessage: 'This is the Inputnum component !',
  },
});
