import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

const Input = styled.input`
  padding: 5px 8px 5px 8px;
  border: 1px solid #bdc3c7;
  width: 60px;
`;

const FlexIn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-left: 15px;
`;

const Flexint = styled.div`
  display: flex;
  flex-direction: row;
`;

const Label = styled.label`
  font-family: sans-serif;
  font-weigh: bold;
  color: #7f8c8d;
`;

class Inputnum extends React.PureComponent {
  render() {
    return (
      <Flexint>
        <FlexIn>
          <Label>Habitaciones</Label>
          <Input placeholder="0" type="number"/>
        </FlexIn>
        <FlexIn>
          <Label>Noches</Label>
          <Input placeholder="0" type="number"/>
        </FlexIn>
        <FlexIn>
          <Label>Adultos</Label>
          <Input placeholder="0" type="number"/>
        </FlexIn>
        <FlexIn>
          <Label>Niños</Label>
          <Input placeholder="0" type="number"/>
        </FlexIn>
      </Flexint>
    );
  }
}

Inputnum.propTypes = {

};

export default Inputnum;
