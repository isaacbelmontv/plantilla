import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import styled from 'styled-components';
import Mapa from '../../components/Mapa';
import Input from '../../components/Input';
import Inputnum from '../../components/Inputnum';
import Boton from '../../components/Boton';
import Main from '../../components/Main';
import Detalles from '../../components/Detalles';
import Terminos from '../../components/Terminos';
import Aside from '../../components/Aside';
import Textnav from '../../components/Textnav';
import SunsetWater from '../../components/SunsetWater';


const Body = styled.body`
  margin: 0px;
  padding: 0px;
`;

const ContainerBody = styled.div`
  margin: 10px 100px 10px 100px;
`;

const Container = styled.div`
  font-family: sans-serif;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Flexbody = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
`;

const Dates = styled.div`
  display: flex;
  flex-direction: row;
  color: #7f8c8d;
`;

const Division = styled.div`
  display: flex;
  flex-direction: row;
  width: 79%;
`;

const Section = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
    <div>
      <Body>
        <ContainerBody>
          <Container>
            <Flexbody>
              <Mapa/>
              <Dates><Input/><Inputnum/></Dates>
            </Flexbody>
            <div><Boton/></div>
          </Container>
          <Section>
            <Main />
            <Aside />
          </Section>
          <Division>
            <Detalles/>
            <Terminos/>
          </Division>
          {/*Nueva seccion*/}
          <Textnav/>
          <SunsetWater/>

        </ContainerBody>
      </Body>
    </div>
    );
  }
}
